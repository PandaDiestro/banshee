# Banshee

Multi-purpose scrappy api hack designed to expose weepy (closed) livestream chats to a language-agnostic environment (like an IRC chat).


<div style="text-align: center;">
<img src="media/Banshee.jpg" style="scale: 90%;"/>
</div>

> *Local people knew that this could only mean one thing. For the banshee was known to all as the lone female figure whose cries of despair herald an impending death.*

## How 2 use

First clone this repository

```sh
$ git clone --depth=1 https://gitlab.com/PandaDiestro/banshee.git
```

Then build it using your favorite builder. There is a script in `cmd/` based on the Bun builder which you can run with the bun runtime, but there is nothing absolutely dependent on a specific builder so feel free to build as you best like it.

```sh
$ bun run build
```

### On Firefox-Based Browsers:

Since there are _still_ no official packaged releases, you will have to navigate over to [about:debugging#/runtime/this-firefox](about:debugging#/runtime/this-firefox) and load the extension selecting the `manifest.json` file. You may as well make your own **.XPI** file for a more prolonged use (although not recommended at the current stage of the project).

### On Chromium-Based Browsers:

You should head over to [chrome://extensions/](chrome://extensions/) and then, after enabling the developer mode, load the repository folder as `unpacked` from the extension panel.

### On Safari-Based Browsers:

Idk lmao, use another browser.




