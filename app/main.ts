import {
  waitForElemAvailable,
  waitForElemNewChild,
  MessageElementProcessor,
} from './lib'


/*
So messages are divided in `blocks` where a block can contain multiple messages sent by the same person in a short amount of time.

The tree goes like follows:

Main block container (user and timestamp info) [[ Ss4fHf ]]
|
|
+--Container timestamp and user id container [[ R3Gmyc ]]
|   |
|   +--User id [[ poVWob ]]
|   |
|   +--Timestamp [[ MuzmKe ]]
|
+-- [[ beTDc ]]
    |
    +--Individual message containers (no user info) [[ er6Kjc ]]


WARNING: currently, all classnames, id's etc. are identificator placeholders, the final idea is for them to be changed on runtime by the user
*/

/* This should be as configureable as possible */
const MSGBoxSelector = ".R3Gmyc"
const MSGAriaContainerSelector = ".z38b6"
const MSGSingletonSelector = ".er6Kjc"


waitForElemAvailable(MSGBoxSelector).then(() => {
  console.log("chat is now available")
  waitForElemNewChild(
    MSGAriaContainerSelector,
    MSGSingletonSelector,
    MessageElementProcessor,
  )
})






