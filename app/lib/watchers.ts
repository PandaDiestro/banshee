/*
  TODO
  - Generalice this with event emitters for 'change', 'edit', etc
  - Refactor the complex logic into more short logical callbacks
*/


const getElementFromNodeList = (selector: string, list: NodeList) => {
  if (list.length === 0)
    return undefined

  if (!(list[0] instanceof HTMLElement))
    return undefined

  const nodeElem = Array.from(list).find((node) => {
    const elem: HTMLElement = node as HTMLElement
    if (elem.matches(selector))
      return elem
  })

  if (nodeElem === undefined)
    return undefined

  return nodeElem as HTMLElement
}

export const waitForElemAvailable = (selector: string) => {
  const initOpt: MutationObserverInit = {
    childList: true,
    subtree: true,
  }

  return new Promise<HTMLElement>((resolve) => {
    const observer = new MutationObserver((records: MutationRecord[], _) => {
      records.find((record) => {
        const nodes = Array.from(record.addedNodes)
        if (nodes.length === 0)
          return

        if (!(nodes[0] instanceof HTMLElement))
          return

        const elements = nodes as HTMLElement[]
        elements.forEach((element) => {
          const children = element.querySelector<HTMLElement>(selector)
          if (children !== null) {
            observer.disconnect()
            resolve(children)
          }

          if (element.matches(selector)) {
            observer.disconnect()
            resolve(element)
          }
        })
      })
    })

    observer.observe(document, initOpt)
  })
}

type AnyFunction = (..._: any[]) => any
export const waitForElemNewChild = (parentSelector: string, childSelector: string, messageAfterCallback: AnyFunction) => {
  const initOpt: MutationObserverInit = {
    childList: true,
    subtree: true,
    characterData: true,
  }

  const containerNode = document.querySelector<HTMLElement>(parentSelector)
  if (containerNode === null)
    return

  const observer = new MutationObserver((records: MutationRecord[], _) => {
    records.forEach((record) => {
      if (record.type !== "childList")
        return

      const element = getElementFromNodeList(childSelector, record.addedNodes)
      if (element === undefined)
        return

      messageAfterCallback(element)
    })

  })

  observer.observe(containerNode, initOpt)
}


