type MessageType = {
  author:     string,
  timestamp:  string,
  content:    string,
}

export const MessageElementProcessor = (container: HTMLElement) => {
  const parentElem = container.parentElement
  if (parentElem === null)
    return undefined

  const grandParentElem = parentElem.parentElement
  if (grandParentElem === null)
    return undefined

  const getTextFrom = (queryStr: string, element: HTMLElement) => {
    const selfContent = element.textContent
    if (queryStr === "self")
      return (selfContent === null) ? "" : selfContent

    const temp = element.querySelector<HTMLElement>(queryStr)
    if (temp === null)
      return ""

    const tempStr = temp.textContent
    if (tempStr === null)
      return ""

    return tempStr
  }

  const timeDate = new Date()
  const message: MessageType = {
    author: getTextFrom(".poVWob", grandParentElem),
    timestamp: `${timeDate.getHours()}:${timeDate.getMinutes()}`,
    content: getTextFrom("self", container),
  }

  console.log(message)
}

